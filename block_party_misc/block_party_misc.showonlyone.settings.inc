<?php
// $Id: block_party.settings.inc,v 1.3.2.1 2011/02/08 06:01:00 johnbarclay Exp $

/**
 * @file
 * admin interface for block party module settings
 *
 */

function block_party_misc_showonlyone_form($form, &$form_state) {

  $showonlyone = variable_get('block_party_misc_showonlyone_blocks', array());
  $form['#title'] = 'Block Party "Show Only One" blocks';
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Configue "Show only one" blocks'),
    '#description' => t('Select which block providers per region should "show only one" block instance per region.
      When a block provider module (block, menu, etc.) for a given region is configured
      to "Show Only One", only one block of that type will appear in that region.'),
  );

  $labels = $modules = array_diff(module_implements('block_info'), array('devel','system','user', 'node', 'context_ui'));
  array_walk($labels, create_function('&$val', '$val = $val . " Module";'));
  $module_options = array_combine($modules, $labels);
  //dpm($showonlyone);
  foreach (list_themes() as $theme_key => $theme_object) {
    if (!$theme_key ||  !is_object($theme_object) || $theme_object->status == 0) {
      continue;
    }
    $collapsed = ($theme_key != variable_get('theme_default', NULL));
    $form[$theme_key] = array('#type' => 'fieldset', '#title' => $theme_object->name, '#collapsed' => $collapsed, '#collapsible' => TRUE);
    foreach (system_region_list($theme_key) as $region_name => $region_label) {
      $collapsed = !isset($showonlyone[$theme_key][$region_name]);
      $form[$theme_key][$region_name] = array('#type' => 'fieldset', '#title' => $region_label .' region', '#collapsed' => $collapsed, '#collapsible' => TRUE);
      $form[$theme_key][$region_name][$theme_key . '__'. $region_name] = array(
        '#type' => 'checkboxes',
        '#options' => $module_options,
        '#default_value' => isset($showonlyone[$theme_key][$region_name]) ? $showonlyone[$theme_key][$region_name] : array(),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  $form['#submit'] = array('block_party_misc_showonlyone_submit');
  return $form;

}

/**
 * save options to big array block_party_misc_showonlyone_blocks
 * with keys: theme_key, region_name, module_name
 */

function block_party_misc_showonlyone_submit($form, &$form_state) {
  $showonlyone = array();
  foreach(array_diff_key($form_state['values'],array('submit' => 'submit')) as $tag => $values) {
    $pairs =  explode('__', $tag);
    if (count($pairs) != 2) {
      continue;
    }
    list($theme_key, $region_name) = $pairs;
    if (is_array($values)) {
      foreach ($values as $module_name => $value) {
        if ($value) {
          $showonlyone[$theme_key][$region_name][$module_name] = $value;
          }
      }
    }
  }
  variable_set('block_party_misc_showonlyone_blocks', $showonlyone);
  if ($form_state['submitted']) {
    watchdog('block_party', 'Block Party misc "Show Only One" options updated.');
  }
}
