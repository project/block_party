<?php
// $Id: block_party.settings.inc,v 1.3.2.1 2011/02/08 06:01:00 johnbarclay Exp $

/**
 * @file
 * admin interface for block party module settings
 *
 */
function block_party_misc_configure_form() {
  require_once('block_party_misc.fields.inc');
  foreach(block_party_misc_conf() as $field_name => $field_data) {
    $options[$field_name]  = $field_data['enabled_option'];
  }
  $form['#title'] = "Other";

  $form['enabled_options'] = array('#type' => 'fieldset', '#title' => t('Enabled Functionality'));
  $form['enabled_options']['block_party_misc_enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which features of this module to use?'),
    '#options' => $options,
    '#default_value' => variable_get('block_party_misc_enabled', array()),
  );

  $form['block_party_misc_fieldset_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for block party misc module options fieldset.'),
    '#default_value' => "",
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => variable_get('block_party_misc_fieldset_label', BLOCK_PARTY_MISC_FIELDSET_LABEL),
  );





  $form = system_settings_form($form);
  return $form;
}
