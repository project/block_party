<?php


function block_party_fieldsets_configure_form($form_state) {

 // Retrieve role names for columns.
  $role_names = user_roles();
  if (block_party_conf('ignore_built_in_roles')) {
    unset($role_names[2]);
    unset($role_names[1]);
  }

  $current_values = variable_get('block_party_fieldset', array());
  
  foreach (block_party_fieldsets() as $fieldset_id => $fieldset) {

    $collapsed = TRUE;
    $form[$fieldset_id] = array(
      '#type' => 'fieldset',
      '#title' => t($fieldset_id),
      '#collapsible' => TRUE,
    );

    foreach ($role_names as $rid => $name) {
      $field_id = join('__', array($fieldset_id, $rid));
      $title = "Fieldset Visibility for $name";

      $default_value = (array)@$current_values[$fieldset_id][$rid];
      $form[$fieldset_id][$field_id] = array(
        '#type' => 'checkboxes',
        '#title' => $title,
        '#default_value' => $default_value,
        '#options' => block_party_fieldsets_options(),
        '#attributes' => array('class' => array('container-inline')),
      );

      if (@array_sum(@array_values($form[$fieldset_id][$field_id]['#default_value'])) > 0) {
        $collapsed = FALSE;
      }
    }

    $form[$fieldset_id]['#collapsed'] = $collapsed;

  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Fieldset Settings'));
  $form['#submit'][] = 'block_party_fieldsets_configure_form_submit';

  return $form;
}

function block_party_fieldsets_configure_form_submit($form, &$form_state) {

	$values = $form_state['values'];
  $store = array();
	foreach($values as $id => $data) {
    $id_parts = explode('__', $id);
    if (count($id_parts) == 2 && is_numeric($id_parts[1])) {  // good enough check.  extra values wont hurt
      $store[$id_parts[BLOCK_PARTY_FIELDSET_FIELDSET_ID_POS]][$id_parts[BLOCK_PARTY_FIELDSET_RID_POS]] = $data;
    }
	}
	variable_set('block_party_fieldset', $store);
	drupal_set_message(t('block party fieldset settings saved.'));
  $form_state['redirect'] = 'admin/config/user-interface/block-party/fieldsets';
	
}


