<?php
// $Id: block_party.settings.inc,v 1.3.2.1 2011/02/08 06:01:00 johnbarclay Exp $

/**
 * @file
 * admin interface for block party module settings
 *
 */

function block_party_contextual_configure_form() {
  require_once('block_party_contextual.fields.inc');
  foreach(block_party_contextual_conf() as $field_name => $field_data) {
    $options[$field_name]  = $field_data['enabled_option'];
  }
  $form['#title'] = "Configure Contextual Menus";
  
  $form['enabled_options'] = array('#type' => 'fieldset', '#title' => t('Enabled Contextual Menus'));
  $form['enabled_options']['block_party_contextual_enabled'] = array( 
    '#type' => 'checkboxes', 
    '#title' => t('Which contextual menus should be added to blocks?'),
    '#options' => $options, 
    '#default_value' => variable_get('block_party_contextual_enabled', array()), 
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'block_party_contextual_configure_form_submit';
  return $form;
}

function block_party_contextual_configure_form_submit($form, &$form_state) {
  if ($form_state['submitted']) {
    watchdog('block_party', 'Block Party misc settings updated.');
  }
}

